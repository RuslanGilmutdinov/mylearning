import java.util.Date;
import java.util.Timer;
import java.util.regex.Pattern;

public class RomanNumerals {

    private static final Pattern ROMAN = Pattern.compile("^(?=.)M*(C[MD]|D?C{0,3})" + "(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$");

    public static boolean isRomanNumeral(String s){
        return ROMAN.matcher(s).matches();
    }

    private static long sum(){
        long timeStart = System.currentTimeMillis();
        //with long sum = 0l; more than 10 times faster than with Long sum = 0l;
        long sum = 0l;

        for(long i =0; i < Integer.MAX_VALUE; i++){
            sum+=i;
        }

        System.out.println(System.currentTimeMillis() - timeStart);
        return sum;
    }


    public static void main(String[] args) {
//        System.out.println(isRomanNumeral("XX"));


    }
}
