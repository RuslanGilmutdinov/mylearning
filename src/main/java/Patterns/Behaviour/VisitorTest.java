package Patterns.Behaviour;

//from javarush
//Используется для упрощения операций над группировками связанных объектов.


interface Visitor {
    void visit(SportCar sportCar);
    void visit(Engine engine);
    void visit(Whell whell);
}
interface CarVis {
    void accept(Visitor visitor);
}
class Engine implements CarVis {
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
class Whell implements CarVis {
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
class SportCar implements CarVis {
    CarVis[] cars;
    public SportCar(){
        cars = new CarVis[]{new Engine(), new Whell()};
    }
    public void accept(Visitor visitor) {
        for (int i = 0; i < cars.length; i++) {
            cars[i].accept(visitor);
        }
        visitor.visit(this);
    }
}
class CarVisitor implements Visitor {
    public void visit(SportCar computer) {
        print("car");
    }
    public void visit(Engine engine) {
        print("engine");
    }
    public void visit(Whell whell) {
        print("whell");
    }
    private void print(String string) {
        System.out.println(string);
    }
}

public class VisitorTest {//тест
    public static void main(String[] args) {
        CarVis computer = new SportCar();
        computer.accept(new CarVisitor());
    }
}