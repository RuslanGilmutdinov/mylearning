package Patterns.Behaviour;

//from javarush
//Позволяет определить грамматику простого языка для проблемной области.


interface Expression {
    String interpret(ContextInter context);
}
class ContextInter {
    public String getLowerCase(String s){
        return s.toLowerCase();
    }
    public String getUpperCase(String s){
        return s.toUpperCase();
    }
}
class LowerExpression implements Expression {
    private String s;
    public LowerExpression(String s) {
        this.s = s;
    }
    public String interpret(ContextInter context) {
        return context.getLowerCase(s);
    }
}
class UpperExpression implements Expression {
    private String s;
    public UpperExpression(String s) {
        this.s = s;
    }
    public String interpret(ContextInter context) {
        return context.getUpperCase(s);
    }
}

public class InterpreterTest {//тест
    public static void main(String[] args) {
        String str = "TesT";
        ContextInter context = new ContextInter();
        Expression lowerExpression = new LowerExpression(str);
        str = lowerExpression.interpret(context);
        System.out.println(str);
        Expression upperExpression = new UpperExpression(str);
        str = upperExpression.interpret(context);
        System.out.println(str);
    }
}