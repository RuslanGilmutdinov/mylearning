package Patterns.Behaviour;

//from javarush
//Позволяет инкапсулировать различные операции в отдельные объекты.

interface Command {
    void execute();
}
class CarCom {
    public void startEngine() {
        System.out.println("запустить двигатель");
    }
    public void stopEngine() {
        System.out.println("остановить двигатель");
    }
}
class StartCar implements Command {
    CarCom car;
    public StartCar(CarCom car) {
        this.car = car;
    }
    public void execute() {
        car.startEngine();
    }
}
class StopCar implements Command {
    CarCom car;
    public StopCar(CarCom car) {
        this.car = car;
    }
    public void execute() {
        car.stopEngine();
    }
}
class CarInvoker {
    public Command command;
    public CarInvoker(Command command){
        this.command = command;
    }
    public void execute(){
        this.command.execute();
    }
}

public class CommandTest {//тест
    public static void main(String[] args) {
        CarCom car = new CarCom();
        StartCar startCar = new StartCar(car);
        StopCar stopCar = new StopCar(car);
        CarInvoker carInvoker = new CarInvoker(startCar);
        carInvoker.execute();
    }
}