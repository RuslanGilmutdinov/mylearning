package Patterns.Structuring;

//from javarush
//Добавляет новые функциональные возможности существующего объекта без привязки его структуры

interface CarDecor {
    void draw();
}
class SportCarDecorator implements CarDecor {
    public void draw() {
        System.out.println("SportCar");
    }
}
class UnknownCarDecorator implements CarDecor {
    public void draw() {
        System.out.println("UnknownCar");
    }
}
abstract class CarDecorator implements CarDecor {
    protected CarDecor decorated;
    public CarDecorator(CarDecor decorated){
        this.decorated = decorated;
    }
    public void draw(){
        decorated.draw();
    }
}
class BlueCarDecorator extends CarDecorator {
    public BlueCarDecorator(CarDecor decorated) {
        super(decorated);
    }
    public void draw() {
        decorated.draw();
        setColor();
    }
    private void setColor(){
        System.out.println("Color: red");
    }
}

public class DecoratorTest {//тест
    public static void main(String[] args) {
        CarDecor sportCar = new SportCarDecorator();
        CarDecor blueUnknownCar = new BlueCarDecorator(new UnknownCarDecorator());
        sportCar.draw();
        System.out.println();
        blueUnknownCar.draw();
    }
}
