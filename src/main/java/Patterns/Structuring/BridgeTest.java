package Patterns.Structuring;

//from javarush
//Разделяет реализацию и абстракцию, дает возможность изменять их свободно друг от друга.
// Делает конкретные классы независимыми от классов реализации интерфейса

interface EngineBridge {
    void setEngine();
}
abstract class CarBridge {
    protected EngineBridge engine;
    public CarBridge(EngineBridge engine){
        this.engine = engine;
    }
    abstract public void setEngine();
}
class SportCarBridge extends CarBridge {
    public SportCarBridge(EngineBridge engine) {
        super(engine);
    }
    public void setEngine() {
        System.out.print("SportCar engine: ");
        engine.setEngine();
    }
}
class UnknownCarBridge extends CarBridge {
    public UnknownCarBridge(EngineBridge engine) {
        super(engine);
    }
    public void setEngine() {
        System.out.print("UnknownCar engine: ");
        engine.setEngine();
    }
}
class SportEngineBridge implements EngineBridge {
    public void setEngine(){
        System.out.println("sport");
    }
}
class UnknownEngineBridge implements EngineBridge {
    public void setEngine(){
        System.out.println("unknown");
    }
}
public class BridgeTest {//тест
    public static void main(String[] args) {
        CarBridge sportCar = new SportCarBridge(new SportEngineBridge());
        sportCar.setEngine();
        System.out.println();
        CarBridge unknownCar = new UnknownCarBridge(new UnknownEngineBridge());
        unknownCar.setEngine();
    }
}
