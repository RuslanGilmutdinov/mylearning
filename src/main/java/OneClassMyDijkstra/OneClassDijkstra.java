package OneClassMyDijkstra;

import DijkstraAlgo.Dijkstra;
import DijkstraAlgo.Graph;
import DijkstraAlgo.Node;

import java.util.*;

public class OneClassDijkstra {
    static class Node {

        Integer distance;
        String name;
        LinkedList<Node> path = new LinkedList<>();
        Map<Node, Integer> neighbors = new HashMap<>();

        @Override
        public String toString() {
            return "MyNode{" +
                    "distance=" + distance +
                    ", name='" + name + '\'' +
                    ", path=" + path +
                    ", neighbors=" + neighbors +
                    '}';
        }


        public Node(String name) {
            this.name = name;
        }


        public void addNeighbor(Node neighbour, Integer distance) {
            neighbors.put(neighbour, distance);
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getDistance() {
            return distance;
        }

        public void setDistance(Integer distance) {
            this.distance = distance;
        }

        public LinkedList<Node> getPath() {
            return path;
        }

        public void setPath(LinkedList<Node> path) {
            this.path = path;
        }

        public Map<Node, Integer> getNeighbors() {
            return neighbors;
        }

        public void setNeighbors(Map<Node, Integer> neighbors) {
            this.neighbors = neighbors;
        }
    }

    static class Graph {

        private Set<Node> nodes = new HashSet<>();

        public void addNode(Node node) {
            nodes.add(node);
        }

        public Set<Node> getNodes() {
            return nodes;
        }

        @Override
        public String toString() {
            return "MyGraph{" +
                    "nodes=" + nodes +
                    '}';
        }

        public void setNodes(Set<Node> nodes) {
            this.nodes = nodes;
        }

        public Graph calculateDistances(Graph graph, Node source) {
            source.setDistance(0);


            Map<Node, Integer> knownNodes = new HashMap<>();
            knownNodes.put(source, 0);

            Set<Node> unknownNodes = new HashSet<>();
            for (Node node : graph.getNodes()) {
                if (node != source) {
                    node.setDistance(Integer.MAX_VALUE);
                    unknownNodes.add(node);
                }
            }

//            System.out.println("knownSize: " + knownNodes.size() + " known: " + knownNodes);
//            System.out.println("unknownSize: " + unknownNodes.size() + " unknown: " + unknownNodes);
            while (unknownNodes.size() != 0) {
                Node nearestNode = null;
                int minDistance = Integer.MAX_VALUE;

                for (Node node : knownNodes.keySet()) {
                    int currentDistance = node.distance;
                    for (Node neighbour : node.neighbors.keySet()) {
                        if (!knownNodes.containsKey(neighbour)) {
                            if (node.neighbors.get(neighbour) + currentDistance < minDistance) {
                                minDistance = node.neighbors.get(neighbour) + currentDistance;
                                nearestNode = neighbour;
                                neighbour.setDistance(minDistance);
                                nearestNode.setDistance(minDistance);
                            }
                        }
                    }
                }

                if (nearestNode != null) {
                    knownNodes.put(nearestNode, minDistance);
                    unknownNodes.remove(nearestNode);
                }
//                System.out.println("knownSize: " + knownNodes.size() + " known: " + knownNodes);
//                System.out.println("unknownSize: " + unknownNodes.size() + " unknown: " + unknownNodes);
            }


            return graph;

        }
    }


    public static void main(String[] args) {
        Node nodeA = new Node("A");
        Node nodeB = new Node("B");
        Node nodeC = new Node("C");
        Node nodeD = new Node("D");
        Node nodeE = new Node("E");
        Node nodeF = new Node("F");

        nodeA.addNeighbor(nodeB, 10);
        nodeA.addNeighbor(nodeC, 15);

        nodeB.addNeighbor(nodeD, 12);
        nodeB.addNeighbor(nodeF, 15);

        nodeC.addNeighbor(nodeE, 10);

        nodeD.addNeighbor(nodeE, 2);
        nodeD.addNeighbor(nodeF, 1);

        nodeF.addNeighbor(nodeE, 5);

        Graph graph = new Graph();

        graph.addNode(nodeA);
        graph.addNode(nodeB);
        graph.addNode(nodeC);
        graph.addNode(nodeD);
        graph.addNode(nodeE);
        graph.addNode(nodeF);


        graph = graph.calculateDistances(graph, nodeA);


        for (Node node : graph.getNodes()) {
            System.out.println("nodeName: " + node.getName() + " distance: " + node.getDistance());
        }
    }
}
