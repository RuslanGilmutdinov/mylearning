package Mephi;

import java.util.Scanner;

public class Sdacha {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int xR = scanner.nextInt();
        int yK = scanner.nextInt();
        int aR = scanner.nextInt();
        int bK = scanner.nextInt();

//        System.out.println("xR: " + xR + " yK: " + yK + " aR: " + aR + " bK: " + bK );

        int R10 = 10;
        int R5 = 5;
        int R1 = 1;
        int K50 = 50;
        int K10 = 10;
        int K1 = 1;

        int res[] = new int[6];

        while (aR * 100 + bK >= xR * 100 + yK) {
            aR -= 10;
            res[0] += 1;
        }
        aR += 10;
        res[0] -= 1;

        while (aR * 100 + bK >= xR * 100 + yK) {
            aR -= 5;
            res[1] += 1;
        }
        aR += 5;
        res[1] -= 1;

        while (aR * 100 + bK >= xR * 100 + yK) {
            aR -= 1;
            res[2] += 1;
        }
        aR += 1;
        res[2] -= 1;

        while (aR * 100 + bK >= xR * 100 + yK) {
            if (bK < 50) {
                bK = bK + 50;
                aR -= 1;
            } else {
                bK -= 50;
            }
            res[3] += 1;
        }

        res[3] -= 1;
        if (bK == 50) {
            bK = 0;
            aR += 1;
        } else if (bK > 50) {
            bK = bK - 50;
            aR += 1;
        } else if (bK < 50) {
            bK += 50;
        }


        while (aR * 100 + bK >= xR * 100 + yK) {
            if (bK < 10) {
                bK = 100 - (10 - bK);
                aR -= 1;
            } else {
                bK -= 10;
            }
            res[4] += 1;
        }

        res[4] -= 1;
        if (bK == 90) {
            bK = 0;
            aR += 1;
        } else if (bK > 90) {
            bK = 10 - (100 - bK);
            aR += 1;
        } else {
            bK += 10;
        }

        while (aR * 100 + bK != xR * 100 + yK) {
            if (bK == 0) {
                bK = 99;
                aR -= 1;
            } else {
                bK -= 1;
            }

            res[5] += 1;
        }


        System.out.println(res[5] + " " + res[4] + " " + res[3] + " " + res[2] + " " + res[1] + " " + res[0]);

    }
}
