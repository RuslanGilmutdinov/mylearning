package Mephi;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Paradox {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        int[] res = {0};

        Map<Integer, Integer> mapa = new HashMap<>();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (!(i == (x) && j == (y))) {
                    int sum = Math.abs(i - x) + Math.abs(j - y);
                    if (mapa.containsKey(sum)) {
//                        System.out.println("i: " + i + " j: " + j + " sum: " + sum);
                        mapa.put(sum, mapa.get(sum) + 1);
                    } else {
//                        System.out.println("i: " + i + " j: " + j + " sum: " + sum);
                        mapa.put(sum, 1);
                    }
                }
            }
        }

//        System.out.println(mapa);
        mapa.values().stream()
                .forEach(count -> {
                    if (count != 1) {
                        res[0] += count * (count - 1) / 2;
                    }
                });

        System.out.println(res[0]);


    }
}
