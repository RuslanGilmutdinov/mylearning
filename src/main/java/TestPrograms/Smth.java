package TestPrograms;

public class Smth {

    public static void main(String[] args) throws InterruptedException {
        Singleton singleton = Singleton.getInstance();


        Thread[] threads = new Thread[1000];
        for (int i = 0; i < 1000; i++) {
            threads[i] = new Thread(() -> {
                Singleton.getInstance();
            });
            threads[i].start();
        }

        for (int i = 0; i < 1000; i++){
            threads[i].join();
        }

        System.out.println(singleton.counter);


    }

}


class Singleton {
    private static final Object sync = new Object();
    private static volatile Singleton instance = null;
    public static int counter;

    private Singleton() {
//        System.out.println("constr");
        counter++;
    }

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (sync) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
