import java.io.*;
import java.util.WeakHashMap;

public class CloneBySerialization implements Serializable{

    private String name;
    private String color;
    private int age;

    public CloneBySerialization(String name, String color, int age) {
        this.name = name;
        this.color = color;
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "CloneBySerialization{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", age=" + age +
                '}';
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException {

        CloneBySerialization obj = new CloneBySerialization("name", "red", 22);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream ous = new ObjectOutputStream(baos);
        //сохраняем состояние кота Васьки в поток и закрываем его(поток)
        ous.writeObject(obj);
        ous.close();
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);

        CloneBySerialization clone = (CloneBySerialization) ois.readObject();

        System.out.println(obj);
        System.out.println(clone);

        System.out.println("**************");

        clone.color = "Orange";

        System.out.println(obj);
        System.out.println(clone);
    }
}
