package DijkstraAlgo;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Node {

    Integer distance;
    String name;
    LinkedList<Node> path = new LinkedList<>();
    Map<Node, Integer> neighbors = new HashMap<>();

    @Override
    public String toString() {
        return "MyNode{" +
                "distance=" + distance +
                ", name='" + name + '\'' +
                ", path=" + path +
                ", neighbors=" + neighbors +
                '}';
    }


    public Node(String name) {
        this.name = name;
    }


    public void addNeighbor(Node neighbour, Integer distance) {
        neighbors.put(neighbour, distance);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public LinkedList<Node> getPath() {
        return path;
    }

    public void setPath(LinkedList<Node> path) {
        this.path = path;
    }

    public Map<Node, Integer> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(Map<Node, Integer> neighbors) {
        this.neighbors = neighbors;
    }
}
