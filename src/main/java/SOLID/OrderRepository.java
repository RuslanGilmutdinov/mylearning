package SOLID;

public interface OrderRepository {
    boolean save(Order order);
}
