package SOLID;

public class OrderProcessor {

    private MailSender mailSender;
    private OrderRepository orderRepository;

    public OrderProcessor(MailSender mailSender, OrderRepository orderRepository) {
        this.mailSender = mailSender;
        this.orderRepository = orderRepository;
    }

    public void process(Order order) {


        if (order.isValid() && orderRepository.save(order)) {
            mailSender.sendConfirmationEmail(order);
        }
    }

}
