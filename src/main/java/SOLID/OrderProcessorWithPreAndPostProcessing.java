package SOLID;

public class OrderProcessorWithPreAndPostProcessing extends OrderProcessor{

    @Override
    public void process(Order order) {
        beforProcessing();
        super.process(order);
        afterProcessing();
    }

    private void afterProcessing() {
        //some post actions
    }

    private void beforProcessing() {
        //some pre actions
    }
}
