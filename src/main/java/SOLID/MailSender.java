package SOLID;

public interface MailSender {
    void sendConfirmationEmail(Order order);
}
