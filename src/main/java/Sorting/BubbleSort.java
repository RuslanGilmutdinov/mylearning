package Sorting;//    Best case:     O(n)
//    Average case:  O(n^2)
//    Worst case:    O(n^2)

public class BubbleSort {

    public static void main(String[] args) {
        int[] arr = {100, 40, 90, 10, 12, 5};




        for (int el : arr) {
            System.out.println(el);
        }


    }


    public static int[] BubbleSorting(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    exchange(arr, i, j);
                }
            }
        }
        return arr;
    }


    public static int[] BubbleSorting2(int[] arr) {

        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    exchange(arr, i, i + 1);
                    sorted = false;
                }
            }
        }


        return arr;
    }


    public static void exchange(int[] arr, int firstIndex, int secondIndex) {
        int temp = arr[secondIndex];
        arr[secondIndex] = arr[firstIndex];
        arr[firstIndex] = temp;
    }


}
