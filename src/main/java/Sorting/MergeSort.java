package Sorting;//        Best case: O(nlogn)
//        Average case: O(nlogn)
//        Worst case: O(nlogn)

//        Space : O(n)


import java.util.Arrays;

public class MergeSort {

    public static void main(String[] args) {
        int[] arr = {3, 100, 40, 90, 10, 12, 5, 0, 9, 10};
        MergeSort(arr, 0, arr.length - 1);

        for (int el : arr) {
            System.out.println(el);

        }
    }


    public static int[] MergeSort(int[] arr, int start, int end) {

        int mid = (start + end) / 2;
        if (start < end) {
            // Sort left half
            System.out.println("left mergesort start " + start + " " + end);
            MergeSort(arr, start, mid);
            System.out.println("left mergesort finish " + start + " " + end);
            // Sort right half
            System.out.println("right mergesort start " + start + " " + end);
            MergeSort(arr, mid + 1, end);
            System.out.println("right mergesort finish " + start + " " + end);
            // Merge left and right half
            merge(arr, start, mid, end);
        }


        return arr;
    }


    public static void merge(int[] arr, int start, int mid, int end) {
        System.out.println("merge " + start + " " + mid + " " + end);

        // Initializing temp array and index
        int[] tempArray = new int[arr.length];
        int tempArrayIndex = start;

        int startIndex = start;
        int midIndex = mid + 1;

        // It will iterate until smaller list reaches to the end
        while (startIndex <= mid && midIndex <= end) {
            if (arr[startIndex] < arr[midIndex]) {
                tempArray[tempArrayIndex++] = arr[startIndex++];
            } else {
                tempArray[tempArrayIndex++] = arr[midIndex++];
            }
        }

        System.out.println(Arrays.toString(arr) + " " + startIndex + " " + midIndex + " " + mid);
        System.out.println(Arrays.toString(tempArray));
        // Copy remaining elements
        while (startIndex <= mid) {
            tempArray[tempArrayIndex++] = arr[startIndex++];
        }
        while (midIndex <= end) {
            tempArray[tempArrayIndex++] = arr[midIndex++];
        }

        // Copy tempArray to actual array after sorting
        for (int i = start; i <= end; i++) {
            arr[i] = tempArray[i];
        }


    }
}
