package Sorting;//        Best Case :    O(n log n)
//        Average Case : O(n log n)
//        Worst Case :   O(n^2)

public class QuickSort {

    public static void main(String[] args) {
        int[] arr = {3, 100, 40, 90, 10, 12, 5, 0, 9, 10};

        QuickSort(arr, 0, arr.length - 1);

        for (int i : arr) {
            System.out.println(i);
        }


    }

    public static void exchange(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }


    public static int[] QuickSort(int[] arr, int left, int right) {
        int i = left;
        int j = right;
        // find pivot number, we will take it as mid
        int pivot = arr[left + (right - left) / 2];

        while (i <= j) {
            // In each iteration, we will increment left until we find element greater than pivot
            // We will decrement right until we find element less than pivot
            while (arr[i] < pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }

            if (i <= j) {
                exchange(arr, i, j);
                //move index to next position on both sides
                i++;
                j--;
            }
        }
// call quickSort() method recursively
        if (left < j)
            QuickSort(arr, left, j);
        if (i < right) {
            QuickSort(arr, i, right);
        }

        return arr;
    }
}
