package Sorting;//        Best case :    O(N^2)
//        Average case : O(N^2)
//        Worst case :   O(N^2)

public class SelectionSort {


    public static void main(String[] args) {
        int[] arr = {100, 40, 90, 10, 12, 5, 0, 9, 10};


//        SelectionSorting(arr);
        S(arr);

        for (int el : arr) {
            System.out.println(el);
        }

    }


    public static int[] SelectionSorting(int[] arr) {

        for (int i = 0; i < arr.length - 1; i++) {
            int index = i;

            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[index]) {
                    int smaller = arr[j];
                    arr[j] = arr[index];
                    arr[index] = smaller;

                }

            }
        }

        return arr;
    }

    public static int[] S(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            int index = i;


            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[index]) {
                    BubbleSort.exchange(arr, j, index);
                }
            }
        }

        return arr;
    }


}
