package Sorting;//        Best case:     O(n)
//        Average case:  O(n^2)
//        Worst case:    O(n^2)

public class InsertionSort {

    public static void main(String[] args) {
        int[] arr = {100, 40, 90, 10, 12, 5};

//        InsertionSorting(arr);

        I(arr);

        for (int el : arr) {
            System.out.println(el);
        }

    }


    public static int[] InsertionSorting(int[] arr) {

        for (int i = 1; i < arr.length; i++) {
            int value = arr[i];
            int j;

            for (j = i; j > 0 && arr[j - 1] > value; j--) {
                arr[j] = arr[j - 1];
            }
            arr[j] = value;
        }

        return arr;
    }

    public static int[] I(int[] arr) {

        for (int i = 1; i < arr.length; i++) {
            int j;
            int value = arr[i];

            for (j = i; j > 0 && arr[j - 1] > value; j--){
                arr[j] = arr[j - 1];
            }
            arr[j] = value;
        }

        return arr;
    }


}
