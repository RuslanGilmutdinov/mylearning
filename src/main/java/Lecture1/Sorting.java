package Lecture1;

public class Sorting {

    public void bubbleSort(int[] a, int length) {
        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < length - 1; i++) {
                if (a[i] > a[i + 1]) {
                    int tmp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = tmp;
                    sorted = false;
                }
            }
        }
    }


    public void insertion(int[] a, int length) {
        for (int i = length - 1; i > 0; i--) {
            if (a[i - 1] > a[i]) {
                int tmp = a[i - 1];
                a[i - 1] = a[i];
                a[i] = tmp;
            }
        }
        for (int i = 2; i < length; i++) {
            int j = i;
            int tmp = a[i];

            while (tmp < a[j - 1]) {
                a[j] = a[j - 1];
                j--;
            }
            a[j] = tmp;
        }
    }


    public void choice(int[] a, int length) {
        for (int i = 0; i < length - 1; i++) {
            int tmpMin = a[i];
            int num = i;
            for(int j = i; j < length; j++){
                if(tmpMin > a[j]){
                    tmpMin = a[j];
                    num = j;
                }
            }
            a[num] = a[i];
            a[i] = tmpMin;

        }
    }


    public static void main(String[] args) {
        int[] a = {2, 4, 1, 6, 0, 5};
        Sorting sorting = new Sorting();
        sorting.choice(a, a.length);

        for (int i : a) {
            System.out.print(i);
        }
    }
}
