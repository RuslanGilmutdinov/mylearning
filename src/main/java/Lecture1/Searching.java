package Lecture1;

public class Searching {

    //на вход подается отсортированный по возрастанию массив
    public int binarySearch(int val, int[] a, int left, int right) {
        if (left >= right) return val == a[left] ? left : -1;
        int mid = (left + right) / 2;
        if (a[mid] == val) {
            return mid;
        }
        if (a[mid] < val) return binarySearch(val, a, mid + 1, right);
        else {
            return binarySearch(val, a, left, mid);
        }
    }

    public int binarySearchIter(int val, int[] a, int left, int right) {
        while (left < right) {
            int mid = (left + right) / 2;
            if (a[mid] == val) return mid;
            if (a[mid] < val) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return a[left] == val ? left : -1;
    }

    public int ternarySearch(int val, int[] a, int left, int right) {
        if(left >= right) return a[left] == val ? left : -1;
        int mid1 = (2 * left + right) / 3;
        int mid2 = (left + 2* right) / 3;
        if(val < a[mid1]) return ternarySearch(val, a, left, mid1 -1);
        else if(val == a[mid1]) return mid1;
        else if(val < a[mid2]) return ternarySearch(val, a, mid1 + 1, mid2 - 1);
        else if(val == a[mid2]) return mid2;
        else{
            return ternarySearch(val, a, mid2 + 1, right);
        }
    }

    public static void main(String[] args) {
        int[] a = {4, 2, 6, 1, 0, 9};

        Sorting sorting = new Sorting();
        sorting.choice(a, a.length);

        Searching searching = new Searching();
        System.out.println(searching.ternarySearch(9, a, 0, a.length - 1));

    }


}
