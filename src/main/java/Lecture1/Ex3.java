package Lecture1;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int pow = scanner.nextInt();
        int points = scanner.nextInt();
        int mod = scanner.nextInt();

        ArrayList<Integer> powCoeff = new ArrayList<>();
        ArrayList<Integer> pointsArray = new ArrayList<>();
        int[] resultArray = new int[points];
        for (int i = 0; i < points; i++) {
            resultArray[i] = 0;
        }
        for (int i = 0; i < pow + 1; i++) {
            powCoeff.add(scanner.nextInt());
        }
        for (int i = 0; i < points; i++) {
            pointsArray.add(scanner.nextInt());
        }
        for (int j = 0; j < points; j++) {
            for (int i = 0; i < pow + 1; i++) {
                resultArray[j] += powCoeff.get(i) * Math.pow(pointsArray.get(j), pow - i);
            }
        }

        for (int el : resultArray) {
            System.out.println(el % mod);
        }
    }
}
