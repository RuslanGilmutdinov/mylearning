package Lecture1;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Ex1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Integer> arrayListA = new ArrayList<Integer>();
        ArrayList<Integer> arrayListB = new ArrayList<Integer>();
        ArrayList<Integer> result = new ArrayList<Integer>();

        Integer a = scanner.nextInt();
        while (a != 0) {
            arrayListA.add(a);
            a = scanner.nextInt();
        }
        a = scanner.nextInt();
        while (a != 0) {
            arrayListB.add(a);
            a = scanner.nextInt();
        }

        System.out.println(arrayListA);
        System.out.println(arrayListB);

        for (int elemA : arrayListA) {
            if (arrayListB.contains(elemA) == false) {
                result.add(elemA);
            }
        }
        for (int elemB : arrayListB) {
            if (arrayListA.contains(elemB) == false) {
                result.add(elemB);
            }
        }


       result.sort(new Comparator<Integer>() {
            public int compare(Integer o1, Integer o2) {
                if (o1 == o2) return 0;
                else if (o1 < o2) return -1;
                else return 1;
            }
        });

        System.out.println(result);
    }
}
