package Lecture1;

import java.util.ArrayList;
import java.util.Scanner;

public class Ex2 {

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> arrayListA = new ArrayList<>();
        ArrayList<Integer> arrayListB = new ArrayList<>();

        int capA = scanner.nextInt();
        int capB = scanner.nextInt();

        for(int i = 0; i < capA; i++){
            arrayListA.add(scanner.nextInt());
        }
        for(int i = 0; i < capB; i++){
            arrayListB.add(scanner.nextInt());
        }

        System.out.println(arrayListA);
        System.out.println(arrayListB);

        int result = 0;

        for(int elem : arrayListA){
            if(arrayListB.contains(elem)){
                result = result + 1;
                arrayListB.remove(arrayListB.indexOf(elem));
            }
        }

        System.out.println(result);

    }
}
