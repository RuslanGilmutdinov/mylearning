package Lecture1;


public class BinarySearchTree {

    private int diam;


    public static class TreeNode {

        int data;
        TreeNode left;
        TreeNode right;

        TreeNode(int data) {
            this.data = data;
        }
    }

    public static boolean search(TreeNode root, TreeNode nodeToBeSearched) {
        if (root == null) {
            return false;
        }
        if (root.data == nodeToBeSearched.data) {
            return true;
        }
        boolean result = false;
        if (root.data > nodeToBeSearched.data) {
            return search(root.left, nodeToBeSearched);
        } else if (root.data < nodeToBeSearched.data) {
            return search(root.right, nodeToBeSearched);
        }

        return result;
    }

    public static TreeNode insert(TreeNode root, TreeNode nodeToBeInserted) {
        if (root == null) {
            root = nodeToBeInserted;
            return root;
        }
        if (root.data > nodeToBeInserted.data) {
            if (root.left == null) {
                root.left = nodeToBeInserted;
                return root;
            } else {
                insert(root.left, nodeToBeInserted);
            }
        } else if (root.data < nodeToBeInserted.data) {
            if (root.right == null) {
                root.right = nodeToBeInserted;
                return root;
            } else {
                insert(root.right, nodeToBeInserted);
            }
        }
        return root;

    }


    public static void preOrder(TreeNode root) {
        if (root == null) {
            return;
        }
//        LinkedList<Integer> res = new LinkedList<>()
        System.out.println(root.data + " ");
        if (root.left != null) {
            preOrder(root.left);
        }
        if (root.right != null) {
            preOrder(root.right);
        }
    }

    public static void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }

        if (root.left != null) {
            inOrder(root.left);
        }
        System.out.println(root.data + " ");
        if (root.right != null) {
            inOrder(root.right);
        }
    }

    public static void postOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        if (root.left != null) {
            postOrder(root.left);
        }
        if (root.right != null) {
            postOrder(root.right);
        }
        System.out.println(root.data);
    }

//    public static int diameter(TreeNode root, int res) {
//        if (root == null) {
//            return 0;
//        }
//
//        if (root.left != null) {
//            res++;
//            diameter(root.left, res);
//        }
//        System.out.println("after left: " + res);
//        if (root.right != null) {
//            res++;
//            diameter(root.right, res);
//        }
//        System.out.println("after right: " + res);
//
//
//        return res;
//    }


    public static TreeNode createBinarySearchTree() {
        TreeNode rootNode = new TreeNode(40);
        TreeNode node20 = new TreeNode(20);
        TreeNode node10 = new TreeNode(10);
        TreeNode node30 = new TreeNode(30);
        TreeNode node60 = new TreeNode(60);
        TreeNode node50 = new TreeNode(50);
        TreeNode node70 = new TreeNode(70);
//        TreeNode node5=new TreeNode(5);
//        TreeNode node55=new TreeNode(55);

        insert(null, rootNode);
        insert(rootNode, node20);
        insert(rootNode, node10);
        insert(rootNode, node30);
        insert(rootNode, node60);
        insert(rootNode, node50);
        insert(rootNode, node70);
//        insert(rootNode,node5);
//        insert(rootNode,node55);
        return rootNode;
    }


    public static void main(String[] args) {


        Object o = new Object();
        o.hashCode();

//        TreeNode rootNode = createBinarySearchTree();
//        postOrder(rootNode);
//
//        int res = 0;
//        System.out.println(diameter(rootNode, res));
    }
}
