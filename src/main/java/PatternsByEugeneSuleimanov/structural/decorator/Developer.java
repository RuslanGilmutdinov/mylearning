package PatternsByEugeneSuleimanov.structural.decorator;

public interface Developer {
    public String makeJob();
}
