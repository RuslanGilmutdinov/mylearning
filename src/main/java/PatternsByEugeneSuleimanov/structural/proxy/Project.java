package PatternsByEugeneSuleimanov.structural.proxy;

public interface Project {
    void run();
}
