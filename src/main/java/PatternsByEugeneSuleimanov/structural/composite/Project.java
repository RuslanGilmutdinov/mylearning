package PatternsByEugeneSuleimanov.structural.composite;

public class Project {
    public static void main(String[] args) {
        Team team = new Team();

        Developer firstDev = new JavaDeveloper();
        Developer secondDev = new JavaDeveloper();
        Developer thirdDev = new CppDeveloper();

        team.addDeveloper(firstDev);
        team.addDeveloper(secondDev);
        team.addDeveloper(thirdDev);

        team.createProject();
    }
}
