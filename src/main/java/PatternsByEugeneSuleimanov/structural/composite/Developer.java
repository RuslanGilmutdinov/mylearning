package PatternsByEugeneSuleimanov.structural.composite;

public interface Developer {
    void writeCode();
}
