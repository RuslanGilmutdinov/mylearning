package PatternsByEugeneSuleimanov.structural.bridge;

public interface Developer {
    public void writeCode();
}
