package PatternsByEugeneSuleimanov.structural.adapter;

public class JavaApplication {
    public void saveObject() {
        System.out.println("save Object ...");
    }

    public void updateObject() {
        System.out.println("update Object ...");
    }

    public void loadObject() {
        System.out.println("load Object");
    }

    public void deleteObject() {
        System.out.println("delete Object");
    }
}
