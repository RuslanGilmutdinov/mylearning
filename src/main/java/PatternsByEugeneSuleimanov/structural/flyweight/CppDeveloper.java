package PatternsByEugeneSuleimanov.structural.flyweight;

public class CppDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("Cpp Developer writes code ...");
    }
}
