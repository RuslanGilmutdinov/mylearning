package PatternsByEugeneSuleimanov.structural.flyweight;

public interface Developer {
    void writeCode();
}
