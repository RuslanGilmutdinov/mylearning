package PatternsByEugeneSuleimanov.creational.factory;

public class Program {
    public static void main(String[] args) {
        DeveloperFactory developerFactory = createDeveloperBySpeciality("java");
        Developer developer = developerFactory.createDeveloper();
        developer.writeCode();
    }


    static DeveloperFactory createDeveloperBySpeciality(String spec) {
        if (spec.equalsIgnoreCase("java")) {
            return new JavaDeveloperFactory();
        } else if (spec.equalsIgnoreCase("cpp")) {
            return new CppDeveloperFactory();
        } else {
            throw new RuntimeException(spec + " is unknown speciality");
        }
    }
}
