package PatternsByEugeneSuleimanov.creational.factory;

public class CppDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("Cpp dev");
    }
}
