package PatternsByEugeneSuleimanov.creational.factory;

public interface Developer {
    void writeCode();
}
