package PatternsByEugeneSuleimanov.creational.factory;

public interface DeveloperFactory {
    Developer createDeveloper();
}
