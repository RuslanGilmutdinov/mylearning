package PatternsByEugeneSuleimanov.creational.prototype;

public interface Copyable {
    Object copy();
}
