package PatternsByEugeneSuleimanov.creational.abstractfactory;

import PatternsByEugeneSuleimanov.creational.abstractfactory.website.WebsiteTeamFactory;



public class AuctionsiteProject {
    public static void main(String[] args) {
        ProjectTeamFactory projectTeamFactory = new WebsiteTeamFactory();
        Developer developer = projectTeamFactory.getDeveloper();
        Tester tester = projectTeamFactory.getTester();
        ProjectManager projectManager = projectTeamFactory.getProjectManager();

        System.out.println("Creating auction");
        developer.writeCode();
        tester.testCode();
        projectManager.manageProject();
    }
}
