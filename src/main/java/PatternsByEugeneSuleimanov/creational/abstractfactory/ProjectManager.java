package PatternsByEugeneSuleimanov.creational.abstractfactory;

public interface ProjectManager {
    void manageProject();
}
