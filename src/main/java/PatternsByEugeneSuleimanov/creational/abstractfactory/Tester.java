package PatternsByEugeneSuleimanov.creational.abstractfactory;

public interface Tester {
    void testCode();
}
