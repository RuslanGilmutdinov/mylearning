package PatternsByEugeneSuleimanov.creational.abstractfactory.website;

import PatternsByEugeneSuleimanov.creational.abstractfactory.Tester;

public class ManualTester implements Tester {
    @Override
    public void testCode() {
        System.out.println("Manual Tester tests code ...");
    }
}
