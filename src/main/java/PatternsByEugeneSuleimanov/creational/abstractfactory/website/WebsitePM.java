package PatternsByEugeneSuleimanov.creational.abstractfactory.website;

import PatternsByEugeneSuleimanov.creational.abstractfactory.ProjectManager;

public class WebsitePM implements ProjectManager {
    @Override
    public void manageProject() {
        System.out.println("Website manager manages project ...");
    }
}
