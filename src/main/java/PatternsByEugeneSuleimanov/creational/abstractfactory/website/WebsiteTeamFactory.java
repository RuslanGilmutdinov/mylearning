package PatternsByEugeneSuleimanov.creational.abstractfactory.website;

import PatternsByEugeneSuleimanov.creational.abstractfactory.Developer;
import PatternsByEugeneSuleimanov.creational.abstractfactory.ProjectManager;
import PatternsByEugeneSuleimanov.creational.abstractfactory.ProjectTeamFactory;
import PatternsByEugeneSuleimanov.creational.abstractfactory.Tester;

public class WebsiteTeamFactory implements ProjectTeamFactory {
    @Override
    public Developer getDeveloper() {
        return new PHPDeveloper();
    }

    @Override
    public Tester getTester() {
        return new ManualTester();
    }

    @Override
    public ProjectManager getProjectManager() {
        return new WebsitePM();
    }
}
