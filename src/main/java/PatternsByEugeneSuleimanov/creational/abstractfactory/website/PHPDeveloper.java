package PatternsByEugeneSuleimanov.creational.abstractfactory.website;

import PatternsByEugeneSuleimanov.creational.abstractfactory.Developer;

public class PHPDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("PHP developer writes code ...");
    }
}
