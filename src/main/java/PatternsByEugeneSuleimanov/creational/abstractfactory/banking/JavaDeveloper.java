package PatternsByEugeneSuleimanov.creational.abstractfactory.banking;

import PatternsByEugeneSuleimanov.creational.abstractfactory.Developer;

public class JavaDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("Java Developer writes code ...");
    }
}
