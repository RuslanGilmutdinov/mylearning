package PatternsByEugeneSuleimanov.creational.abstractfactory.banking;

import PatternsByEugeneSuleimanov.creational.abstractfactory.ProjectManager;

public class BankingPM implements ProjectManager {
    @Override
    public void manageProject() {
        System.out.println("BankingPM manages project ...");
    }
}
