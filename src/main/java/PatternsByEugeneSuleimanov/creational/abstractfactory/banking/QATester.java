package PatternsByEugeneSuleimanov.creational.abstractfactory.banking;

import PatternsByEugeneSuleimanov.creational.abstractfactory.Tester;

public class QATester implements Tester {
    @Override
    public void testCode() {
        System.out.println("QA tests code ...");
    }
}
