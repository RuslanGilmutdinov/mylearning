package PatternsByEugeneSuleimanov.creational.abstractfactory.banking;

import PatternsByEugeneSuleimanov.creational.abstractfactory.Developer;
import PatternsByEugeneSuleimanov.creational.abstractfactory.ProjectManager;
import PatternsByEugeneSuleimanov.creational.abstractfactory.ProjectTeamFactory;
import PatternsByEugeneSuleimanov.creational.abstractfactory.Tester;

public class BankingTeamFactory implements ProjectTeamFactory {
    @Override
    public Developer getDeveloper() {
        return new JavaDeveloper();
    }

    @Override
    public Tester getTester() {
        return new QATester();
    }

    @Override
    public ProjectManager getProjectManager() {
        return new BankingPM();
    }
}
