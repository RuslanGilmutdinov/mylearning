package PatternsByEugeneSuleimanov.creational.abstractfactory;

public interface Developer {
    void writeCode();
}
