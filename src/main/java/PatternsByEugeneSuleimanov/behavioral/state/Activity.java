package PatternsByEugeneSuleimanov.behavioral.state;

public interface Activity {
    void justDoIt();
}
