package PatternsByEugeneSuleimanov.behavioral.strategy;

public interface Activity {
    void justDoIt();
}
