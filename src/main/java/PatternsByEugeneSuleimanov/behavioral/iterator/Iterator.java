package PatternsByEugeneSuleimanov.behavioral.iterator;

public interface Iterator {
    boolean hasNext();

    Object next();
}
