package PatternsByEugeneSuleimanov.behavioral.iterator;

public interface Collection {
    Iterator getIterator();
}
