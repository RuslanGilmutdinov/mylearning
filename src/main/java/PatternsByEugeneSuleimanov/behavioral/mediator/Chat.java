package PatternsByEugeneSuleimanov.behavioral.mediator;

public interface Chat {
    void sendMessage(String message, User user);
}
