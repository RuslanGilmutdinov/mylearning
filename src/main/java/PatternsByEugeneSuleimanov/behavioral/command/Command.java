package PatternsByEugeneSuleimanov.behavioral.command;

public interface Command {
    void execute();
}
