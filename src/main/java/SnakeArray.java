public class SnakeArray {


    public static void getSnakeArr(int n) {
        int statement = 0;
        int[][] result = new int[n][n];
        boolean[][] helper = new boolean[n][n];

        int i = 0;
        int j = 0;
        for (int c = 1; c <= n * n; c++) {


            if (helper[i][j] != true) {
                result[i][j] = c;
                helper[i][j] = true;
            }


            if (statement == 0) {
                if (j + 1 < n && helper[i][j + 1] == false) {
                    statement = 0;
                } else if ((j + 1 == n || helper[i][j + 1] == true) && (i + 1 == n || helper[i + 1][j] == true) && (j - 1 < 0 || helper[i][j - 1] == true) && (i - 1 < 0 || helper[i - 1][j] == true)) {
                    statement = 4;
                } else {
                    statement = 1;
                }
            } else if (statement == 1) {
                if ((j + 1 == n || helper[i][j + 1] == true) && (i + 1 < n && helper[i + 1][j] == false)) {
                    statement = 1;
                } else {
                    statement = 2;
                }
            } else if (statement == 2) {
                if ((j + 1 == n || helper[i][j + 1] == true) && (i + 1 == n || helper[i + 1][j] == true) && (j - 1 >= 0 && helper[i][j - 1] == false)) {
                    statement = 2;
                } else if ((j + 1 == n || helper[i][j + 1] == true) && (i + 1 == n || helper[i + 1][j] == true) && (j - 1 < 0 || helper[i][j - 1] == true) && (i - 1 < 0 || helper[i - 1][j] == true)) {
                    statement = 4;
                } else {
                    statement = 3;
                }

            } else if (statement == 3) {
                if ( (i - 1 >= 0 && helper[i - 1][j] == false)) {
                    statement = 3;
                }
//                if ((j + 1 == n || helper[i][j + 1] == true) && (i + 1 == n || helper[i + 1][j] == true) && (j - 1 < 0 || helper[i][j - 1] == true) && (i - 1 >= 0 && helper[i - 1][j] == false)) {
//                    statement = 3;
//                }
                else if (j + 1 < n && helper[i][j + 1] == false) {
                    statement = 0;
                }
//                else if((j + 1 == n || helper[i][j + 1] == true) && (i + 1 == n || helper[i + 1][j] == true) && (j - 1 < 0 || helper[i][j - 1] == true) && (i - 1 < 0 || helper[i - 1][j] == true)) {
//                    statement = 4;
//                }
            }
//                else if ((j + 1 == n || helper[i][j + 1] == true) && (i + 1 == n || helper[i + 1][j] == true) && (j - 1 < 0 || helper[i][j - 1] == true) && (i - 1 < 0 || helper[i - 1][j] == true)) {
//                    statement = 4;
//                }

            System.out.println("statement: " + statement);
            switch (statement) {
                case 0:
                    j++;
                    break;
                case 1:
                    i++;
                    break;
                case 2:
                    j--;
                    break;
                case 3:
                    i--;
                    break;
                case 4:
                    break;
            }
        }

        printArr(result);

    }


    public static void printArr(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.print("\n");
        }
    }


    public static void main(String[] args) {
        getSnakeArr(5);

    }


}
