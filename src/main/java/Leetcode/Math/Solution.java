package Leetcode.Math;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

    }




    public int romanToInt(String s) {
        Map<Character, Integer> mapa = new HashMap<>();
        mapa.put('I', 1);
        mapa.put('V', 5);
        mapa.put('X', 10);
        mapa.put('L', 50);
        mapa.put('C', 100);
        mapa.put('D', 500);
        mapa.put('M', 1000);


        int res = 0;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'M') {
                res += mapa.get('M');
            } else if (s.charAt(i) == 'D') {
                res += mapa.get('D');
            } else if (s.charAt(i) == 'C') {
                if (i + 1 < s.length()) {
                    if (s.charAt(i + 1) == 'M') {
                        res += 900;
                        i++;
                    } else if (s.charAt(i + 1) == 'D') {
                        res += 400;
                        i++;
                    } else {
                        res += mapa.get('C');
                    }
                } else {
                    res += mapa.get('C');
                }
            } else if (s.charAt(i) == 'L') {
                res += mapa.get('L');
            } else if (s.charAt(i) == 'X') {
                if (i + 1 < s.length()) {
                    if (s.charAt(i + 1) == 'C') {
                        res += 90;
                        i++;
                    } else if (s.charAt(i + 1) == 'L') {
                        res += 40;
                        i++;
                    } else {
                        res += mapa.get('X');
                    }
                } else {
                    res += mapa.get('X');
                }
            } else if (s.charAt(i) == 'V') {
                res += 5;
            } else if (s.charAt(i) == 'I') {
                if (i + 1 < s.length()) {
                    if (s.charAt(i + 1) == 'X') {
                        res += 9;
                        i++;
                    } else if (s.charAt(i + 1) == 'V') {
                        res += 4;
                        i++;
                    } else {
                        res += mapa.get('I');
                    }
                } else {
                    res += mapa.get('I');
                }
            }


        }

        return res;

    }

    public boolean isPowerOfThree(int n) {
        if (n % 3 != 0 && n != 1) {
            return false;
        } else if (n == 1) {
            return true;
        } else if (n == 0) {
            return false;
        } else {
            return isPowerOfThree(n / 3);
        }
        ///alternative, no loop and no recursion:
//        if(n==0) return false;

//        return n == Math.pow(3, Math.round(Math.log(n)/Math.log(3)));

    }

    public List<String> fizzBuzz(int n) {
        List<String> res = new LinkedList<>();

        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                res.add("FizzBuzz");
//                break;
            } else if (i % 3 == 0) {
                res.add("Fizz");
            } else if (i % 5 == 0) {
                res.add("Buzz");
            } else {
                res.add(String.valueOf(i));
            }
        }
        return res;

    }

}
