package Leetcode.Array;


import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Solution solution = new Solution();
        for (int elem : solution.twoSum(new int[]{0, 4, 3, 0}, 0)) {
            System.out.println(elem);
        }

    }

    public void rotate(int[][] matrix) {
        int n = matrix.length;
        for (int i = 0; i < n / 2; i++) {
            for (int j = 0; j < n / 2; j++) {
                int temp1 = matrix[j][n - 1 - i];
                int temp2 = matrix[n - 1 - i][n - 1 - j];
                int temp3 = matrix[n - 1 - j][i];
                matrix[j][n - 1 - i] = matrix[i][j];
                matrix[i][j] = temp3;
                matrix[n - 1 - j][i] = temp2;
                matrix[n - 1 - i][n - 1 - j] = temp1;
            }
        }
    }

    public boolean isValidSudoku(char[][] board) {

        if (checkLines(board, 1) == false) {
            return false;
        }
        if (checkLines(board, 0) == false) {
            return false;
        }
        return checkBlocks(board, 0, 0) &&
                checkBlocks(board, 3, 0) &&
                checkBlocks(board, 6, 0) &&
                checkBlocks(board, 0, 3) &&
                checkBlocks(board, 3, 3) &&
                checkBlocks(board, 6, 3) &&
                checkBlocks(board, 0, 6) &&
                checkBlocks(board, 3, 6) &&
                checkBlocks(board, 6, 6);

    }

    public boolean checkBlocks(char[][] board, int l, int c) {
        Set<Character> chars = new HashSet<>();
        for (int i = l; i < l + 3; i++) {

            for (int j = c; j < c + 3; j++) {
                if (Character.isDigit(board[i][j])) {
                    if (chars.contains(board[i][j])) {
                        return false;
                    } else {
                        chars.add(board[i][j]);
                    }
                }
            }
        }
        return true;

    }

    public boolean checkLines(char[][] board, int flag) {
        for (int i = 0; i < 9; i++) {
            Set<Character> chars = new HashSet<>();
            for (int j = 0; j < 9; j++) {


                //check lines
                if (flag == 1) {
                    if (Character.isDigit(board[i][j])) {
                        if (chars.contains(board[i][j])) {
                            return false;
                        } else {
                            chars.add(board[i][j]);
                        }
                    }
                }
                //check columns
                else if (flag == 0) {
                    if (Character.isDigit(board[j][i])) {
                        if (chars.contains(board[j][i])) {
                            return false;
                        } else {
                            chars.add(board[j][i]);
                        }
                    }
                }
            }
        }
        return true;

    }

    public int[] twoSum(int[] nums, int target) {

        Map<Integer, Integer> mapa = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (mapa.containsKey(nums[i])) {
                mapa.put(nums[i], mapa.get(nums[i]) + 1);
            } else {
                mapa.put(nums[i], 1);
            }
        }

        for (int i = 0; i < nums.length; i++) {
            mapa.put(nums[i], mapa.get(nums[i]) - 1);
            if (mapa.containsKey(target - nums[i]) && mapa.get(target - nums[i]) > 0) {
//                System.out.println("i: " + i + " " + Arrays.binarySearch(nums, i + 1, nums.length, target - nums[i]));
                return new int[]{i, findIndex(nums, target - nums[i], i + 1)};
            }
        }
        return null;
    }

    public int findIndex(int[] arr, int elem, int startIndex) {
        for (int i = startIndex; i < arr.length; i++) {
            if (arr[i] == elem) {
                return i;
            }

        }
        return -1;
    }

    public int[] plusOne(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            if (digits[i] < 9) {
                digits[i]++;
                return digits;
            }
            digits[i] = 0;
        }
        int[] res = new int[digits.length + 1];
        res[0] = 1;
        return res;
    }

    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (set.contains(num)) {
                return true;
            } else {
                set.add(num);
            }
        }
        return false;
    }

    public int removeDuplicates(int[] nums) {
        int index = 1;
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] != nums[i + 1]) {
                nums[index++] = nums[i];
            }
        }
        return index;
    }
}
