package Leetcode;

import Lecture1.ListNode;

import javax.sound.sampled.EnumControl;
import java.util.*;

public class Leetcode {

    public static void main(String[] args) {
//        System.out.println(divider(123));
//        int[] arr = {0, 1, 0, 3, 12};
//        System.out.println(isHappy(2));
//        moveZeros(arr);
//        System.out.println(Arrays.toString(arr));

//        int[] arr = {1, 1, 2, 2, 5, 5, 7, 7};

//        System.out.println(countElements(arr));
//        System.out.println(backspaceCompare("ab##", "c#d#"));
//        System.out.println(stringShift("wpdhhcj", new int[][]{new int[]{0, 7}, new int[]{1, 7}, new int[]{1, 0}, new int[]{1, 3}, new int[]{0, 3}, new int[]{0, 6}, new int[]{1, 2}}));

//        System.out.println(checkValidString("**((*"));

//        System.out.println(minPathSum(new int[][]{new int[]{1, 3, 1}, new int[]{1, 5, 1}, new int[]{4, 2, 1}}));
//        System.out.println(searchInRotated(new int[]{4, 5, 6, 7, 0, 1, 2}, 0));
//        int[] arr = {10, 5, 3, 6, 13, 16, 7};
        //new int[]{4, 5, 6, 7, 0, 1, 2}
//        System.out.println(findMinimaIndex(arr, 0, 6));
//        int[] arr = new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4};
//        System.out.println(maxSubArray(arr));


//
//        char[][] grid = new char[][]{new char[]{'1', '1', '1', '1', '0'}, new char[]{'1', '1', '0', '1', '0'}, new char[]{'1', '1', '0', '0', '0'}, new char[]{'0', '0', '0', '0', '0'}};
//
//
//        System.out.println(numIslands(grid));
    }


    public static int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0) {
            return 0;
        }

        int numOfIslands = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {
                    numOfIslands += checkNeighbourhoods(grid, i, j);
                }
            }
        }
        return numOfIslands;
    }

    public static int checkNeighbourhoods(char[][] grid, int i, int j) {
        if (i < 0 || j < 0 || i >= grid.length || j >= grid[i].length || grid[i][j] == '0') {
            return 0;
        }

        grid[i][j] = '0';
        checkNeighbourhoods(grid, i + 1, j);
        checkNeighbourhoods(grid, i - 1, j);
        checkNeighbourhoods(grid, i, j + 1);
        checkNeighbourhoods(grid, i, j - 1);

        return 1;

    }


    public static int maxSubArray(int[] arr) {
        int[] tempArr = new int[arr.length];
        int max = arr[0];
        tempArr[0] = arr[0];


        for (int i = 1; i < arr.length; i++) {
            tempArr[i] = Math.max(arr[i], arr[i] + tempArr[i - 1]);
            max = Math.max(max, tempArr[i]);
        }

        return max;

    }


    public static int searchInRotated(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        int mid;
        while (left <= right) {
            mid = (left + right) / 2;

            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] >= nums[left]) {
                if (target <= nums[mid] && target >= nums[left]) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            } else {
                if (target >= nums[mid] && target <= nums[right]) {
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }

            }
        }
        return -1;
    }

    public static int findMinimaIndex(int[] arr, int start, int end) {
        int mid = start + (end - start) / 2;
        int size = arr.length;

        /*check the local minima
         *first check if left and right neighbor exists */
        if ((mid == 0 || arr[mid - 1] > arr[mid]) &&
                (mid == size - 1 || arr[mid + 1] > arr[mid]))
            return mid;
            /* check if left neighbor is less than mid element, if yes go left */
        else if (mid > 0 && arr[mid] > arr[mid - 1]) {
            return findMinimaIndex(arr, start, mid);
        } else {
            /*check if right neighbor is greater than mid element, if yes go right */
            return findMinimaIndex(arr, mid + 1, end);
        }
    }

    public static int minPathSum(int[][] grid) {

        if (grid == null || grid.length == 0) {
            return 0;
        }

        int res[][] = new int[grid.length][grid[0].length];

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                res[i][j] += grid[i][j];
                if (i > 0 && j > 0) {
                    res[i][j] += Math.min(grid[i - 1][j], grid[i][j - 1]);
                } else if (i > 0) {
                    res[i][j] += grid[i - 1][j];
                } else if (j > 0) {
                    res[i][j] += grid[i][j - 1];
                }
            }
        }
        return res[grid.length - 1][grid[0].length - 1];
    }


    public static boolean checkValidString(String s) {
        if (s.length() < 1) {
            return true;
        }

        int balance = 0;

        for (int i = 0; i < s.length(); ++i) {
            if (s.toCharArray()[i] == ')') {
                balance--;
            } else {
                balance++;
            }
            if (balance < 0) {
                return false;
            }
        }
        if (balance == 0) {
            return true;
        }

        balance = 0;
        for (int i = s.length() - 1; i >= 0; --i) {
            if (s.toCharArray()[i] == '(') {
                balance--;
            } else {
                balance++;
            }
            if (balance < 0) {
                return false;
            }
        }
        return true;


    }
//    public static boolean checkValidString(String s) {
//        // '(' in s equals 0 in LinkedList
//        // ')' in s equals 1 in LinkedList
//        LinkedList<Integer> linkedList = new LinkedList<>();
//        int counterStars = 0;
//
//        for (char c : s.toCharArray()) {
//            if (c == '(') {
//                linkedList.add(0);
////                else if (linkedList.contains(1)) {
////                    linkedList.remove(linkedList.indexOf(1));
////                }
//            } else if (c == ')') {
//                if (linkedList.contains(0)) {
//                    linkedList.remove(linkedList.indexOf(0));
//                } else if (counterStars >= linkedList.size() + 1) {
//                    linkedList.add(1);
//                } else {
//                    return false;
//                }
//            } else if (c == '*') {
//                counterStars++;
//            }
//            System.out.println(linkedList);
//        }
//
//        System.out.println(linkedList.size());
//        System.out.println(linkedList);
//        if (linkedList.isEmpty()) {
//            return true;
//        } else if (linkedList.size() <= counterStars) {
//            return true;
//        }
//
//
//        return false;
//    }


    public static String stringShift(String s, int[][] shift) {
        int left = 0;
        int right = 0;
        for (int[] command : shift) {
            if (command[0] == 0) {
                left += command[1];
            } else {
                right += command[1];
            }
        }

        int shiftR = (right - left) % s.length();
        if (shiftR < 0) {
            shiftR = s.length() - Math.abs(shiftR);
        }

        System.out.println(shiftR);
        char[] arrS = s.toCharArray();
        shift(arrS, shiftR);
        return String.valueOf(arrS);
    }

    public static void shift(char[] arr, int shift) {
        reverse(arr, 0, arr.length - 1);
        reverse(arr, 0, shift - 1);
        reverse(arr, shift, arr.length - 1);
    }

    public static void reverse(char[] arr, int start, int end) {
        while (start < end) {
            System.out.println(String.valueOf(arr) + " " + start + " " + end);
            char temp = arr[end];
            arr[end] = arr[start];
            arr[start] = temp;
            start++;
            end--;
        }
    }

    public static boolean backspaceCompare(String S, String T) {

        Stack<Character> sStack = new Stack<>();
        for (char c : S.toCharArray()) {
            if (c != '#') {
                sStack.push(c);
            } else if (!sStack.isEmpty()) {
                sStack.pop();
            }
        }
        Stack<Character> tStack = new Stack<>();
        for (char t : T.toCharArray()) {
            if (t != '#') {
                tStack.push(t);
            } else if (!tStack.isEmpty()) {
                tStack.pop();
            }
        }

        while (!sStack.isEmpty()) {
            char current = sStack.pop();
            if (tStack.isEmpty() || tStack.pop() != current) {
                return false;
            }
        }
        return sStack.isEmpty() && tStack.isEmpty();

    }
//    public static boolean backspaceCompare(String S, String T) {
//        int lengthS = S.length();
//        int lengthT = T.length();
//        for (int i = 0; i < lengthS; i++) {
//            if (S.toCharArray()[i] == '#') {
//                S = S.substring(0, i - 1) + S.substring(i + 1, lengthS);
//                lengthS -= 2;
//                i -= 2;
//            }
//            System.out.println("S: " + S + " " + lengthS);
//        }
//        for (int i = 0; i < lengthT; i++) {
//            if (T.toCharArray()[i] == '#') {
//                T = T.substring(0, i - 1) + T.substring(i + 1, lengthT);
//                lengthT -= 2;
//                i -= 2;
//            }
//            System.out.println("T: " + T + " " + lengthT);
//        }
//
//        return T.equals(S);
//    }

    public static ListNode middleNode(ListNode head) {

        return new ListNode(0);


    }


    public static int countElements(int[] arr) {
        int count = 0;
        HashSet<Integer> set = new HashSet<>();

        for (int val : arr) {
            set.add(val);
        }
        System.out.println(set);

        for (int i = 0; i < arr.length; i++) {
            if (set.contains(arr[i] + 1)) {
                count++;
            }
        }
        return count;
    }

    public static void moveZeros(int[] nums) {
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[index++] = nums[i];
            }
        }
        for (int i = index; i < nums.length; i++) {
            nums[i] = 0;
        }

    }

    public static void moveZeros2(int[] arr) {

        int left = 0;
        int right = arr.length - 1;
        while (left <= right) {
            if (arr[left] == 0) {
                swap(arr, left, right--);
            } else {
                left++;
            }
        }
        Arrays.sort(arr, 0, right + 1);
    }

    public static void swap(int[] arr, int first, int second) {
        int temp = arr[second];
        arr[second] = arr[first];
        arr[first] = temp;
    }

    public static boolean isHappy(int n) {
        HashSet<Integer> seen = new HashSet<>();
        while (n != 1) {
            int current = n;
            int sum = 0;
            while (current != 0) {
                sum += (current % 10) * (current % 10);
                current /= 10;
            }
            //it means we are in cycle
            if (seen.contains(sum)) {
                return false;
            }
            seen.add(sum);
            n = sum;

        }
        return true;
    }


//    public static boolean isHappy(int number) {
//
//        try {
//            while (divider(number) != 1) {
//                number = divider(number);
//
//            }
//        }catch (Exception e){
//            return false;
//        }
//        return true;
//    }
//
//    public static int divider(int number) {
//
//        int nuls = 1;
//
//        while (number >= Math.pow(10, nuls)) {
//            nuls++;
//        }
//        System.out.println("nuls: " + nuls);
//        int result = 0;
//        for (int i = nuls; i > 0; i--) {
//            result += (number % 10) * (number % 10);
//            System.out.println("result: " + result);
//            System.out.println("numberb: " + number);
//            number = (number - number % 10) / (10 );
//            System.out.println("numbera: " + number);
//        }
//
//        return result;
//    }

}
