package Leetcode.Strings;

import PatternsByEugeneSuleimanov.behavioral.iterator.Collection;

import javax.naming.InsufficientResourcesException;
import java.util.Arrays;
import java.util.Collections;

public class Solution {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.longestCommonPrefix(new String[]{"dog","racecar","car"}));

    }

    public String longestCommonPrefix(String[] strs) {
        if(strs == null || strs.length == 0){
            return "";
        }

        Arrays.sort(strs, (String s1, String s2) -> {
            if (s1.length() > s2.length()) {
                return 1;
            } else if (s1.length() < s2.length()) {
                return -1;
            } else {
                return 0;
            }
        });

//        for(String str: strs){
//            System.out.println(str);
//        }

        String prefix = "";
        for(int i = 0; i < strs[0].length(); i++){
            for(int j = 0; j < strs.length; j++){
                if(strs[0].charAt(i) != strs[j].charAt(i)){
                    return prefix;
                }else if((strs[0].charAt(i) == strs[j].charAt(i)) && (j == strs.length - 1)){
                    prefix += strs[0].charAt(i);
                }
            }
        }
        return prefix;
    }


    public int strStr(String haystack, String needle) {
        if (haystack == null || needle == null) {
            return 0;
        }

        if (needle.length() == 0) {
            return 0;
        }


        for (int i = 0; i < haystack.length(); i++) {
            if (i + needle.length() > haystack.length()) {
                return -1;
            }

            int m = i;

            for (int j = 0; j < needle.length(); j++) {
                if (needle.charAt(j) == haystack.charAt(m)) {
                    if (j == needle.length() - 1) {
                        return i;
                    }
                    m++;
                } else {
                    break;
                }
            }
        }
        return -1;

    }


    public void reverseString(char[] s) {
        for (int i = 0; i < s.length - i; i++) {
            char temp = s[i];
            s[i] = s[s.length - 1 - i];
            s[s.length - 1 - i] = temp;
        }
    }

    public int reverseInt(int x) {

        int i = x;
        long res = 0;
        while (i != 0) {
            int d = i % 10;
            res = res * 10 + d;
            i = i / 10;
        }
        if (res > Integer.MAX_VALUE) {
            return 0;
        } else {
            return (int) res;

        }
    }

    public int firstUniqChar(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.indexOf(s.charAt(i)) == s.lastIndexOf(s.charAt(i))) {
                return i;
            }
        }
        return -1;

    }

    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        char[] sArr = s.toCharArray();
        char[] tArr = t.toCharArray();
        Arrays.sort(sArr);
        Arrays.sort(tArr);

        return String.valueOf(sArr).equals(String.valueOf(tArr));
    }

    public boolean isPalindrome(String s) {

        s = s
//                .replace(",", "")
//             .replace(":", "")
//             .replace(" ", "")
//             .replace(".", "")
//             .replace("@", "")
//             .replace("#", "")
//             .replace("$", "")
//             .replace("*", "")
//             .replace("%", "")
//             .replace("&", "")
                .toLowerCase();
        System.out.println(s);
        if (s.length() == 0 || s.length() == 1) {
            return true;
        }
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            while (i < j && !Character.isLetterOrDigit(s.charAt(i))) {
                i++;
            }

            while (i < j && !Character.isLetterOrDigit(s.charAt(j))) {
                j--;
            }

            if (i < j && s.charAt(i++) != s.charAt(j--)) {
                return false;
            }
        }
        return true;
    }


    public int myAtoi(String str) {
        if (str.length() == 0) {
            return 0;
        }
        String num = "";
        char[] strArr = str.toCharArray();
        boolean sign = true;
        int i = 0;
        while (i < strArr.length && strArr[i] == ' ') {
            i++;
        }
        if (i < strArr.length && strArr[i] == '-') {
            sign = false;
            i++;
        } else if (i < strArr.length && strArr[i] == '+') {
            sign = true;
            i++;
        } else if (i < strArr.length && !Character.isDigit(strArr[i])) {
            return 0;
        }
        while (i < strArr.length && Character.isDigit(strArr[i])) {
            num += strArr[i++];
        }
        if (num.length() == 0) {
            return 0;
        }

        try {
            if (sign == true) {
                return Integer.parseInt(num);
            } else {
                return -1 * Integer.parseInt(num);
            }
        } catch (NumberFormatException e) {
            if (sign == false) {
                return Integer.MIN_VALUE;
            } else {
                return Integer.MAX_VALUE;
            }
        }

    }
}
