package Leetcode;

import java.util.*;

public class lastStoneWeightSolution {

    public static int lastStoneWeight(int[] stones) {
        List<Integer> list = new ArrayList<>();
        for (int elem : stones) {
            list.add(elem);
        }
//        System.out.println("size: " + list.size());

        while (list.size() > 1) {
//            System.out.println("before  sorting: "+list);
            Collections.sort(list);
//            System.out.println("after  sorting: "+list);
            if (list.get(list.size() - 1) == list.get(list.size() - 2)) {
                list.remove(list.size() - 1);
                list.remove(list.size() - 1);
            } else {
                int localMin = list.get(list.size() - 2);
                int localMax = list.get(list.size() - 1);
                list.remove(list.size() - 1);
                list.remove(list.size() - 1);
                list.add(localMax - localMin);
            }
        }
//        System.out.println(list);
        if (list.size() > 0) {
            return list.get(0);
        } else {
            return 0;
        }
    }


    public static void main(String[] args) {
//        int[] stones = new int[]{2, 7, 4, 1, 8, 1};
        int[] stones = new int[]{2, 2};
        System.out.println(lastStoneWeight(stones));
    }


}
