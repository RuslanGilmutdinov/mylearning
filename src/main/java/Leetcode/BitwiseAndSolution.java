package Leetcode;


public class BitwiseAndSolution {

    public static int rangeBitwiseAnd(int m, int n) {
        int res = 0;

        for (int bit = 30; bit >= 0; bit--) {
            System.out.println("bit: " + bit);
            if ((m & (1 << bit)) != (n & (1 << bit))) {
                System.out.println("break");
                break;
            } else {
                System.out.println("res: " + res);
                res |= m & (1 << bit);
            }
        }


        return res;

    }

    public static void main(String[] args) {


        System.out.println(rangeBitwiseAnd(2, 770));
    }
}
