package Leetcode;

import java.util.*;

public class TwoCitySchedule {

    public int twoCitySchedule(int[][] costs) {

        int res = 0;

        List<Elem> diffs = new LinkedList<>();


        for (int i = 0; i < costs.length; i++) {
            diffs.add(new Elem(costs[i][0] - costs[i][1], costs[i]));
        }
        Collections.sort(diffs, Comparator.comparing(x -> x.diff));

        for (int i = 0; i < diffs.size(); i++) {
            if (i < diffs.size() / 2) {
                res += diffs.get(i).cost[0];
            } else {
                res += diffs.get(i).cost[1];
            }

        }
        return res;

    }


    public class Elem {
        int diff;
        int[] cost;

        public Elem(int diff, int[] cost) {
            this.diff = diff;
            this.cost = cost;
        }
    }

    public static void main(String[] args) {
        int[][] costs = new int[][]{{10, 20}, {30, 200}, {400, 50}, {30, 20}};

        TwoCitySchedule twoCitySchedule = new TwoCitySchedule();
        System.out.println(twoCitySchedule.twoCitySchedule(costs));
    }
}
