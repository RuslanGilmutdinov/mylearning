package Leetcode;

public class InvertBinaryTree {


    public static TreeNode invertTree(TreeNode root) {
        if (root != null) {
            TreeNode temp = root.left;
            root.left = root.right;
            root.right = temp;

            invertTree(root.left);
            invertTree(root.right);
        }

        return root;
    }

    public static boolean isSameTree(TreeNode p, TreeNode q) {

        if(p == null && q != null) return false;
        if(p != null && q == null) return false;
        if(p == null && q == null) return true;


        if (p.val != q.val) return false;


        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    public static void main(String[] args) {
        TreeNode a = new TreeNode(1);
        TreeNode b = new TreeNode(2);
        TreeNode c = new TreeNode(3);
        TreeNode d = new TreeNode(4);
        TreeNode e = new TreeNode(6);
        TreeNode f = new TreeNode(7);
        TreeNode g = new TreeNode(9);

        d.left = b;
        d.right = e;
        b.left = a;
        b.right = c;
        e.left = f;
        e.right = g;


        invertTree(d);


    }
}
