package Leetcode.Others;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.arrangeCoins(5));
    }

    public int missingNumber(int[] nums) {
        int sum = 0;
        for (int elem : nums) {
            sum += elem;
        }
        return nums.length * (nums.length + 1) / 2 - sum;
    }


    public int arrangeCoins(int n) {
       return (int)(Math.sqrt(2 * (long)n + 0.25) - 0.5);

    }
}
