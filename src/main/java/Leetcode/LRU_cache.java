package Leetcode;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LRU_cache {
    private int capacity;

    private LinkedHashMap<Integer, Integer> map;

    public LRU_cache(int capacity) {
        this.capacity = capacity;
        this.map = new LinkedHashMap<>();
    }

    public int get(int key) {

        if (map.containsKey(key)) {
            int res = map.get(key);
            map.remove(key);
            map.put(key, res);
            return res;
        }
        return -1;


    }

    public void put(int key, int value) {
        if (map.containsKey(key)) {
            map.remove(key);
        } else if (map.size() == capacity) {
            map.remove(map.keySet().iterator().next());
        }
        map.put(key, value);
    }


    public static void main(String[] args) {
        Map<Integer, Integer> temp = new LinkedHashMap<>();

        temp.put(1, 1);
        temp.put(2, 2);
        temp.put(4, 4);

        System.out.println(temp);

        temp.remove(temp.keySet().iterator().next());

        System.out.println(temp);


    }

}
