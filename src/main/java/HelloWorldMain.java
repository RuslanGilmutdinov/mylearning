import org.omg.CORBA.INTERNAL;

import java.util.*;

public class HelloWorldMain {

    public static void main(String[] args) {
//        HelloWorld helloWorld = () -> System.out.println("hello");
//        helloWorld.sayHello();

//        String str = "hheekmllo";
//        System.out.println(firstNoRepeated(str));
//        int arr[] = new int[]{12, 56, 76, 89, 100, 343, 21, 234};
//        int[] arr2 = {5, 3, 1, 2};
//        int arr[] = {0, 1, 0, 0, 1, 1, 1, 0, 1};
//        int arr[] = {100, 140, 150, 200, 215, 400};
//        int dep[] = {110, 300, 210, 230, 315, 600};
//        System.out.println(findPlatformRequiredForStation(arr, dep, 6));
//        int arr[] = {20, 40, 50, 40, 50, 20, 30, 30, 50, 20, 40, 40, 20};

//        int[] arr = new int[]{1, 3, -5, 7, 8, 20, -40, 6};

//        PrintPairEqualsToXHash(arr, 15);
//        System.out.println(getOddTimesElementH(arr));
//        sort012(arr);

//        DutchFlagAlgo012(arr);
//        System.out.println(Arrays.toString(arr));

//        System.out.println(Arrays.toString(separateOddAndEven(arr)));
//        System.out.println(findMissingElem(arr2));
//        System.out.println(getMinAndMax(arr).toString());
//        System.out.println(getSecondLargest(arr));


//        int arr[] = {14, 12, 70, 15, 99, 65, 21, 90};
//        int arr[] = {10, 5, 3, 6, 13, 16, 7};
//        int arr[] = {2, 3, 6, 4, 9, 0, 11};
//        int[] arr = {2, 6, -1, 2, 4, 1, -6, 5};
//        solveEfficient(arr, 3);
        int arr[] = {100, 200, 10, 5, 20, 12, 120, 170, 17};
        System.out.println(findPeakElement(arr));

    }


    public static int findPeakElement(int[] arr) {
        int left = 0;
        int right = arr.length - 1;

        while (left < right) {
            int mid = (left + right) / 2;

            if (arr[mid] < arr[mid + 1]) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return arr[left];
    }


    public static int solve(int start, int end, int[] arr) {

        // finding mid for dividing the array into two parts
        int mid = (start + end) / 2;

        // if the mid element is not the corner element and it is greater
        // than or equal to its neighbours
        if ((mid > 0 && mid < arr.length - 1) && (arr[mid] >=
                arr[mid + 1] && arr[mid] >= arr[mid - 1])) {
            return mid;
        }

        // if the mid element is left corner element and it is greater
        //than or equal to its right neighbour.

        else if (mid == 0 && mid != arr.length - 1 && arr[mid] >=
                arr[mid + 1]) {
            return mid;
        }

        // if the mid element is right corner element and it is greater
        // than or equalto its left neighbour.

        else if (mid == arr.length - 1 && mid != 0 &&
                arr[mid - 1] <= arr[mid]) {
            return mid;
        }

        // if mid element is smaller than its left neighbour,
        // then peak element will be in left side for sure.

        else if (mid != 0 && arr[mid - 1] > arr[mid]) {
            return solve(start, mid - 1, arr);
        } else {
            if (mid + 1 <= arr.length - 1) {
                return solve(mid + 1, end, arr);
            }
        }
        // in case the array has only one element then than is the
        //peak element
        return 0;
    }


    //sliding window maximum
    public static void solveEfficient(int[] arr, int k) {
        LinkedList<Integer> deque = new LinkedList<>();

        for (int i = 0; i < arr.length; i++) {

            /* keep removing the elements from deque
             * which are smaller than the current element,
             * because we need to keep our deque sorted in dec order
             */
            while (!deque.isEmpty() && arr[deque.getLast()] <= arr[i]) {
                deque.removeLast();
            }

            /* removing the i-k element, because that element does not belong
             * to the subarray we are currently working on.
             */
            while (!deque.isEmpty() && deque.getFirst() <= i - k) {
                deque.removeFirst();
            }

            deque.addLast(i);

            if (i >= k - 1) {
                /* only print when we have processed atleast k elements
                 * to make the very first subarray
                 */
                System.out.println(deque.toString());
                System.out.println(" " + arr[deque.getFirst()]);
            }

        }
    }


    public static void findSubArrayWithDefinedSum(int[] arr, int target) {
        int start = 0;
        int end = 0;
        int currentSum = 0;

        while (start <= arr.length && end <= arr.length) {
            if (currentSum == target) {
                System.out.println("start index: " + start + ", end index: " + (end - 1));

                if (end <= arr.length - 1) {
                    currentSum += arr[end];
                }
                end++;
            } else {
                if (currentSum > target) {
                    currentSum -= arr[start];
                    start++;
                } else {
                    if (end <= arr.length - 1) {
                        currentSum += arr[end];
                    }
                    end++;
                }
            }
        }
    }

    public static int findLeaderElement(int[] arr) {
        int size = arr.length - 1;
        int leader = arr[size];

        for (int i = size - 1; i > 0; i--) {
            if (arr[i] > leader) {
                leader = arr[i];
            }
        }

        return leader;
    }

    public static int findLocalMinima(int[] arr, int start, int end) {
        int mid = (start + end) / 2;
        int size = arr.length;
        //first check if left and right neighbor exists
        if ((mid == 0 || arr[mid - 1] > arr[mid]) &&
                (mid == size - 1 || arr[mid + 1] < arr[mid])) {
            return arr[mid];
        }
        //check if left neighbor is less than mid element, if yes go left
        else if (mid > 0 && arr[mid] > arr[mid - 1]) {
            return findLocalMinima(arr, start, mid);
        }
        //check if right neighbor is greater than mid element, if yes go right
        else {
            return findLocalMinima(arr, mid + 1, end);
        }


    }

    public static int calculateMaxProfit(int[] arr) {
        int maxProfit = Integer.MIN_VALUE;
        int lowestPriceTillDay = arr[0];

        for (int i = 0; i < arr.length; i++) {
            int profit = 0;
            if (arr[i] > lowestPriceTillDay) {
                profit = arr[i] - lowestPriceTillDay;

                if (profit > maxProfit) {
                    maxProfit = profit;
                }

            } else {
                lowestPriceTillDay = arr[i];
            }
        }

        return maxProfit;
    }


    //Search in a row wise and column wise sorted matrix
    //idea: start from the right high corner
    //if current element < X go to the next row(down)
    //if current element > X go the previous coloumn(left)
    public static void PrintElementFromMatrix(int[][] sortedMatrix, int X) {
        int C = sortedMatrix[0].length;
        int R = sortedMatrix.length;

        int c = C - 1;
        int r = 0;

        while (r <= R && c >= 0) {
            if (sortedMatrix[r][c] < X) {
                r++;
            } else if (sortedMatrix[r][c] > X) {
                c--;
            } else if (sortedMatrix[r][c] == X) {
                System.out.println("It has");
            }
        }
    }

    public static void PrintPairEqualsToXHash(int[] arr, int X) {
        HashMap<Integer, Integer> mapa = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            mapa.put(arr[i], i);
        }

        for (int i = 0; i < arr.length / 2; i++) {
            if (mapa.get(X - arr[i]) != null && mapa.get(X - arr[i]) != i) {
                System.out.println(arr[i] + " " + (X - arr[i]));
            }
        }
    }

    public static void PrintPairEqualsToX(int[] arr, int X) {
        Arrays.sort(arr);

        int left = 0;
        int right = arr.length - 1;

        while (left < right) {
            if (arr[left] + arr[right] < X) {
                left++;
            } else if (arr[left] + arr[right] > X) {
                right--;
            } else if (arr[left] + arr[right] == X) {
                System.out.println(arr[left] + " " + arr[right]);
                left++;
                right--;
            }
        }


    }


    public static void PrintPairWhoseSumIsClosestToX(int[] arr, int X) {
        Arrays.sort(arr);
        int left = 0;
        int right = arr.length - 1;
        int minLeft = 0;
        int minRight = right;
        int minDiff = Integer.MAX_VALUE;

        while (left < right) {
            int currentDiff = Math.abs(arr[left] + arr[right] - X);

            if (currentDiff < minDiff) {
                minDiff = currentDiff;
                minLeft = left;
                minRight = right;
            }

            if (arr[left] + arr[left] < X) {
                left++;
            } else {
                right--;
            }
        }
        System.out.println(arr[minLeft] + " " + arr[minRight]);

    }

    public static void PrintPairWhoseSumIsClosestToZero(int[] arr) {
        Arrays.sort(arr);
        int left = 0;
        int right = arr.length - 1;
        int minSum = Integer.MAX_VALUE;
        int minLeft = left;
        int minRight = right;

        int sum = 0;
        while (left < right) {
            sum = arr[left] + arr[right];

            if (Math.abs(sum) < Math.abs(minSum)) {
                minLeft = left;
                minRight = right;
                minSum = sum;
            }

            if (sum < 0) {
                left++;
            } else {
                right--;
            }
        }

        System.out.println(arr[minLeft] + " " + arr[minRight]);
    }

    public static int findPlatformRequiredForStation(int[] arr, int[] dep, int n) {
        int maxPlatforms = 0;
        int platformNeeded = 0;

        int i = 0;
        int j = 0;
        Arrays.sort(arr);
        Arrays.sort(dep);

        while (i < n && j < n) {
            if (arr[i] < dep[j]) {
                platformNeeded++;
                i++;
                if (platformNeeded > maxPlatforms) {
                    maxPlatforms = platformNeeded;
                }
            } else {
                platformNeeded--;
                j++;
            }
        }
        System.out.println(platformNeeded);
        return maxPlatforms;
    }


    public static int getOddTimesElementH(int ar[]) {
        HashMap<Integer, Integer> mapa = new HashMap<>();
        for (int elem : ar) {
            if (!mapa.containsKey(elem)) {
                mapa.put(elem, 1);
            } else {
                mapa.put(elem, mapa.get(elem) + 1);
            }
        }
        for (Map.Entry<Integer, Integer> set : mapa.entrySet()) {
            if (set.getValue() % 2 == 1) {
                return set.getKey();
            }
        }
        return -1;
    }

    //find the number occurring odd number of times in an array
    //it works if only one number occurring odd times
    public static int getOddTimesElement(int ar[]) {
        int i;
        int result = 0;
        for (i = 0; i < ar.length; i++) {
            //XOR
            result = result ^ ar[i];
        }
        return result;
    }


    public static void DutchFlagAlgo012(int[] arr) {
        int low = 0;
        int mid = 0;
        int high = arr.length - 1;

        while (mid <= high) {
            switch (arr[mid]) {
                case 0:
                    swap(arr, low, mid);
                    mid++;
                    low++;
                    break;
                case 1:
                    mid++;
                    break;
                case 2:
                    swap(arr, mid, high);
                    high--;
                    break;
            }
        }

    }


    public static void sort012(int[] arr) {
        int[] freq = new int[3];
        for (int elem : arr) {
            freq[elem]++;
        }
        int pointer = 0;
        for (int i = 0; i < freq.length; i++) {
            while (freq[i]-- > 0) {
                arr[pointer] = i;
                pointer++;
            }
        }

    }


    public static int[] separateOddAndEven(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            int left = 0;
            int right = arr.length - 1;

            while (arr[left] % 2 == 0) {
                left++;
            }
            while (arr[right] % 2 == 1) {
                right--;
            }

            if (left < right) {
                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
            }
        }
        return arr;
    }


    public static int[] separate0and1s(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int left = 0;
            int right = arr.length - 1;

            while (arr[left] == 0) {
                left++;
            }
            while (arr[right] == 1) {
                right--;
            }

            if (left < right) {
                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
            }

        }
        return arr;

    }


    //given an integer array containing 1 to n
    // but one of the number from 1 to n in the array is missing.
    public static int findMissingElem(int[] arr) {
        int n = arr.length + 1;
        int sum = 0;
        for (int el : arr) {
            sum += el;
        }
        int missing = n * (n + 1) / 2 - sum;
        return missing;
    }


    //find second largest element with O(n) time complexity
    public static int getSecondLargest(int[] arr) {
        int largest = arr[0];
        int secondLargest = arr[0];

        for (int element : arr) {
            if (element > largest) {
                secondLargest = largest;
                largest = element;
            }
            if (element > secondLargest && element < largest) {
                secondLargest = element;
            }
        }
        return secondLargest;
    }


    public static Pair<Integer> getMinAndMax(int[] arr) {
        int smallest = arr[0];
        int largest = arr[0];

        for (int element : arr) {
            if (element < smallest) {
                smallest = element;
            }

            if (element > largest) {
                largest = element;
            }
        }

        return new Pair<>(smallest, largest);
    }


    public static String getMinString(String[] strArr) {
        String minStr = strArr[0];
        for (int i = 1; i < strArr.length; i++) {
            if (strArr[i].length() < minStr.length()) {
                minStr = strArr[i];
            }
        }
        return minStr;
    }

    public static Character firstNoRepeated(String str) {
        Map<Character, Integer> characterIntegerMap = new LinkedHashMap<>();
        for (char ch : str.toCharArray()) {
            if (!characterIntegerMap.containsKey(ch)) {
                characterIntegerMap.put(ch, 1);
            } else {
                characterIntegerMap.put(ch, characterIntegerMap.get(ch) + 1);
            }
        }

        for (Map.Entry<Character, Integer> entry : characterIntegerMap.entrySet()) {
            if (entry.getValue() == 1) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static Character firstNonRepeated(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (str.lastIndexOf(str.charAt(i)) == str.indexOf(str.charAt(i))) {
                return str.charAt(i);
            }

        }
        return null;
    }


    public static void allSubstrings(String str) {
        for (int i = 0; i < str.length(); i++) {
            for (int j = i + 1; j <= str.length(); j++) {
                System.out.println(str.substring(i, j));
            }
        }
    }

    public static int lengthStrUseException(String str) {
        int i = 0;

        try {
            for (i = 0; ; i++) {
                str.charAt(i);
            }
        } catch (Exception e) {

        }

        return i;
    }


    public static boolean isAnagrams2(String str1, String str2) {

        if (str1.length() != str2.length()) {
            return false;
        }

        for (int i = 0; i < str1.length(); i++) {
            char ch = str1.charAt(i);
            int index = str2.indexOf(ch);
            if (index != -1) {
                str2 = str2.substring(0, index) + str2.substring(index + 1, str2.length());
            } else {
                return false;
            }


        }
        return str2.isEmpty();
    }

    public static boolean isAnagrams(String str1, String str2) {
        char[] firstSorted = str1.toCharArray();
        Arrays.sort(firstSorted);
        char[] secondSorted = str2.toCharArray();
        Arrays.sort(secondSorted);


        return String.valueOf(firstSorted).equals(String.valueOf(secondSorted));
    }


    public static String stringBufferReverse(String str) {
        StringBuffer stringBuffer = new StringBuffer(str);
        return stringBuffer.reverse().toString();
    }

    public static String recursiveString(String str) {
        if (str.length() == 1) {
            return str;
        } else {
            return str.charAt(str.length() - 1) + reverseString(str.substring(0, str.length() - 1));
        }
    }


    public static String reverse2(String str) {
        String reversed = "";

        for (int i = str.length() - 1; i >= 0; i--) {
            reversed = reversed + str.charAt(i);
        }

        return reversed;
    }

    public static String reverseString(String str) {

        char[] strChar = str.toCharArray();
        int i, j;

        for (i = 0, j = strChar.length - 1; i < j; i++, j--) {
            char temp = strChar[j];
            strChar[j] = strChar[i];
            strChar[i] = temp;
        }

        str = String.valueOf(strChar);

        return str;
    }

    public static void swap(int[] arr, int first, int second) {
        int temp = arr[second];
        arr[second] = arr[first];
    }

}
