import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortMoviesByName {

    public static void main(String[] args) {


        Movie m1 = new Movie("Inception", 110);
        Movie m2 = new Movie("GodFather", 200);
        Movie m3 = new Movie("Forest Gump", 130);
        Movie m4 = new Movie("Avengers", 150);

        List<Movie> listOfMovies = new ArrayList<>();
        listOfMovies.add(m1);
        listOfMovies.add(m2);
        listOfMovies.add(m3);
        listOfMovies.add(m4);

        for (Movie movie : listOfMovies) {
            System.out.println(movie.toString());
        }
        System.out.println();

        Collections.sort(listOfMovies, (mov1, mov2) -> mov1.getMovieName().compareTo(mov2.getMovieName()));

        for (Movie movie : listOfMovies) {
            System.out.println(movie.toString());
        }


    }


}
