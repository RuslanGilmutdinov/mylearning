package SpringMathFun;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class MathExpression {
    public ArrayList<String> operations;
    public Integer[] numbers;

    @Override
    public String toString() {
        return "MathExpression{" +
                "operations=" + operations +
                ", numbers=" + Arrays.toString(numbers) +
                '}';
    }

    public Integer[] getNumbers() {
        return numbers;
    }

    public void setNumbers(Integer[] numbers) {
        this.numbers = numbers;
    }

    public ArrayList<String> getOperations() {
        return operations;
    }

    public void setOperations(ArrayList<String> operations) {
        this.operations = operations;
    }

    public MathExpression(ArrayList<String> operations, Integer[] numbers) {
        this.operations = operations;
        this.numbers = numbers;
    }

    public MathExpression() {
        this.operations = new ArrayList<String>();
        this.numbers = new Integer[]{};
    }


    public static Integer calculateExpression(MathExpression mathExpression) {
        while (mathExpression.operations.contains("*")) {
//            System.out.println(mathExpression);
            int indexOfOperation = mathExpression.operations.indexOf("*");
            int tempRes = mathExpression.numbers[indexOfOperation] * mathExpression.numbers[indexOfOperation + 1];
            mathExpression.numbers[indexOfOperation] = tempRes;
            mathExpression.setNumbers(removeElem(mathExpression.numbers, indexOfOperation + 1));
//            if (mathExpression.operations.size() > indexOfOperation + 1) {
//                mathExpression.operations.set(indexOfOperation, mathExpression.operations.get(indexOfOperation + 1));
//            }
//            mathExpression.operations.remove(indexOfOperation + 1);
            mathExpression.operations.remove(indexOfOperation);
        }
        while (mathExpression.operations.contains("/")) {
//            System.out.println(mathExpression);
            int indexOfOperation = mathExpression.operations.indexOf("/");
            int tempRes = mathExpression.numbers[indexOfOperation] / mathExpression.numbers[indexOfOperation + 1];
            mathExpression.numbers[indexOfOperation] = tempRes;
            mathExpression.setNumbers(removeElem(mathExpression.numbers, indexOfOperation + 1));
//            if (mathExpression.operations.size() > indexOfOperation + 1) {
//                mathExpression.operations.set(indexOfOperation, mathExpression.operations.get(indexOfOperation + 1));
//            }
//            mathExpression.operations.remove(indexOfOperation + 1);
            mathExpression.operations.remove(indexOfOperation);
        }
        while (mathExpression.operations.contains("+") || mathExpression.operations.contains("-")) {
//            System.out.println("before: " + mathExpression);
            int indexOfPlusOperation = mathExpression.operations.indexOf("+");
            int indexOfMinusOperation = mathExpression.operations.indexOf("-");
//            System.out.println("indexOfPlusOperation: " + indexOfPlusOperation);
//            System.out.println("indexOfMinusOperation: " + indexOfMinusOperation);
            int indexOfNearestOperation = -10;
            int flag = 1;
            if (indexOfMinusOperation == -1) {
                indexOfNearestOperation = indexOfPlusOperation;
                flag = 1;
            }
            if (indexOfPlusOperation == -1) {
                indexOfNearestOperation = indexOfMinusOperation;
                flag = -1;
            }
            if (indexOfMinusOperation * indexOfPlusOperation >= 0 && indexOfMinusOperation + indexOfPlusOperation > 0) {
                if (indexOfMinusOperation < indexOfPlusOperation) {
                    flag = -1;
                } else {
                    flag = 1;
                }
            }
            if (indexOfMinusOperation * indexOfPlusOperation >= 0 && indexOfMinusOperation + indexOfPlusOperation > 0) {
                indexOfNearestOperation = Math.min(indexOfMinusOperation, indexOfPlusOperation);
            }
//            System.out.println("Nearest: " + indexOfNearestOperation);
            int tempRes = mathExpression.numbers[indexOfNearestOperation] + flag * mathExpression.numbers[indexOfNearestOperation + 1];
            mathExpression.numbers[indexOfNearestOperation] = tempRes;
            mathExpression.setNumbers(removeElem(mathExpression.numbers, indexOfNearestOperation + 1));
//            System.out.println("AAA " + mathExpression);
//            if (mathExpression.operations.size() >= indexOfNearestOperation + 1) {
//                mathExpression.operations.set(indexOfNearestOperation, mathExpression.operations.get(indexOfNearestOperation + 1));
//            }
//            System.out.println("AAA " + mathExpression);
//            System.out.println(mathExpression);
//            mathExpression.operations.remove(indexOfNearestOperation + 1);
            mathExpression.operations.remove(indexOfNearestOperation);
        }


        return mathExpression.numbers[0];
    }

    public static Integer[] removeElem(Integer[] arr, int index) {
//        System.out.println("index: " + index);
        Integer[] res = new Integer[arr.length - 1];

        for (int i = 0; i < res.length; i++) {
            if (i < index) {
                res[i] = arr[i];
            } else {
                res[i] = arr[i + 1];
            }
        }

        return res;

    }


    //5 уровней
    //1 - 3 числа, без умножения и деления
    //2 - 3 числа с умножением
    //3 - 4 числа с умножением
    //4 - 4 числа с умножением и делением
    //5 - 5 чисел с умножением и делением

    public static MathExpression generateMathExpression(int level) {
        MathExpression mathExpression = new MathExpression();
        ArrayList<String> operations = new ArrayList<>();
        Integer[] numbers = new Integer[5];
        String[] ops = new String[]{"+", "-", "*", "/"};
        Random random = new Random();
        switch (level) {

            case 2:
                numbers[0] = random.nextInt(10);
                numbers[1] = random.nextInt(10);
                numbers[2] = random.nextInt(10);
                operations.add(ops[random.nextInt(3)]);
                operations.add(ops[random.nextInt(3)]);
                break;
            case 3:
                numbers[0] = random.nextInt(10);
                numbers[1] = random.nextInt(10);
                numbers[2] = random.nextInt(10);
                numbers[3] = random.nextInt(10);
                operations.add(ops[random.nextInt(3)]);
                operations.add(ops[random.nextInt(3)]);
                operations.add(ops[random.nextInt(3)]);
                break;
            case 4:
                numbers[0] = random.nextInt(10);
                numbers[1] = random.nextInt(10);
                numbers[2] = random.nextInt(10);
                numbers[3] = random.nextInt(10);
                operations.add(ops[random.nextInt(4)]);
                operations.add(ops[random.nextInt(4)]);
                operations.add(ops[random.nextInt(4)]);
                break;
            case 5:
                numbers[0] = random.nextInt(10);
                numbers[1] = random.nextInt(10);
                numbers[2] = random.nextInt(10);
                numbers[3] = random.nextInt(10);
                numbers[4] = random.nextInt(10);
                operations.add(ops[random.nextInt(4)]);
                operations.add(ops[random.nextInt(4)]);
                operations.add(ops[random.nextInt(4)]);
                operations.add(ops[random.nextInt(4)]);
                break;
            //default is the first level
            default:
                numbers[0] = random.nextInt(10);
                numbers[1] = random.nextInt(10);
                numbers[2] = random.nextInt(10);
                operations.add(ops[random.nextInt(2)]);
                operations.add(ops[random.nextInt(2)]);
                break;

        }
        mathExpression.setNumbers(numbers);
        mathExpression.setOperations(operations);
        return mathExpression;

    }

    public static void main(String[] args) {

        MathExpression mathExpression = new MathExpression();
        mathExpression.setNumbers(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
        mathExpression.operations.add("*");
        mathExpression.operations.add("+");
        mathExpression.operations.add("*");
        mathExpression.operations.add("-");
        mathExpression.operations.add("*");
        mathExpression.operations.add("*");
        mathExpression.operations.add("/");
        mathExpression.operations.add("/");
        mathExpression.operations.add("+");
        mathExpression.operations.add("/");
//        System.out.println(mathExpression.operations.size());
//        System.out.println(mathExpression);

//        System.out.println(calculateExpression(mathExpression));

//               MathExpression mathExpression1 = generateMathExpression(1);
//        System.out.println(mathExpression1);
//        System.out.println(calculateExpression(mathExpression1));
//        MathExpression mathExpression2 = generateMathExpression(2);
//        System.out.println(mathExpression2);
//        System.out.println(calculateExpression(mathExpression2));
//        MathExpression mathExpression3 = generateMathExpression(3);
//        System.out.println(mathExpression3);
//        System.out.println(calculateExpression(mathExpression3));
//        MathExpression mathExpression4 = generateMathExpression(4);
//        System.out.println(mathExpression4);
//        System.out.println(calculateExpression(mathExpression4));
//        MathExpression mathExpression5 = generateMathExpression(5);
//        System.out.println(mathExpression5);
//        System.out.println(calculateExpression(mathExpression5));
        long[] delays = new long[10000];
        long max = 0;
        for (int i = 0; i < 10000; i++) {
            long start = System.currentTimeMillis();
            int res = -2;
            while (res != 1 || res != 0 || res != -1) {
                res = calculateExpression(generateMathExpression(5));
            }
            long finish = System.currentTimeMillis();
            delays[i] = finish - start;
            if(finish - start > max){
                max = finish - start;
            }
        }
        System.out.println(Arrays.stream(delays).average());
        System.out.println("max: "+ max);

//        System.out.println(mathExpression);

    }
}
