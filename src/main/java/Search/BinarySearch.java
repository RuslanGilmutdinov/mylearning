package Search;

public class BinarySearch {

    public static void main(String[] args) {

    }


    public static int binarySearch(int[] sortedArr, int element) {

        int left = 0;
        int right = sortedArr.length - 1;


        while(left <= right){
            int mid = (left + right) / 2;
            if(sortedArr[mid] < element){
                left = mid + 1;
            }
            else if(sortedArr[mid] > element){
                right = mid - 1;
            }
            else if(sortedArr[mid] == element){
                return mid; //return position
            }
        }
        return -1;


    }


}
